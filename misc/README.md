# Misc Documentation

This directory contains misc. documentation for the M939 series trucks.

Description                                                       | File
------------------------------------------------------------------|----------------------------------------------------------------
Steel Soldiers 5 ton parts list, downloaded 2019-01-14            | [5 Ton Parts List.pdf](5%20Ton%20Parts%20List.pdf)
Parts Cross Reference, downloaded around 2012                     | [5ton6x6PartsReference.pdf](5ton6x6PartsReference.pdf)
ABS MWO Field Service Bulletin                                    | [AMG_ABS_MWO_field_service.pdf](AMG_ABS_MWO_field_service.pdf)
Air brake treadle valve manual                                    | [E7 Dual Brake Valve.pdf](E7%20Dual%20Brake%20Valve.pdf)
Schematic diagram fold-outs excerpted from TMs                    | [M939-Schematics.pdf](M939-Schematics.pdf)
ABS maintenance training                                          | [M939_ABS_maint_sustainment_tng_std_handout.pdf](M939_ABS_maint_sustainment_tng_std_handout.pdf)
ABS diagnostic box manual                                         | [M939_Infocenter_Instruction_Manual_L31204_11-03.pdf](M939_Infocenter_Instruction_Manual_L31204_11-03.pdf)
Tires for the M939 series trucks                                  | [M939_tires.pdf](M939_tires.pdf)
SINCGARS installation in M939 series with MK-2378/VRC             | [TB11-5820-890-20-71.pdf](TB11-5820-890-20-71.pdf)
Vises used on front bumpers of medium wreckers                    | [Wrecker_Vises/README.md](Wrecker_Vises/README.md)
Extract from TB 43-0001-62-03-1, ABS Ground Precautionary Message | [extract_TB43-0001-62-03-1.pdf](extract_TB43-0001-62-03-1.pdf)

---

Crane hoist motor documents downloaded from a Steel Soldiers thread about the [M936 Wrecker Hydraulic Hoist Motor](https://www.steelsoldiers.com/threads/m936-wrecker-hydraulic-hoist-motor.121547/)

Description                                                       | File
------------------------------------------------------------------|----------------------------------------------------------------
Commercial Intertech motor tech manual                            | [crane winch motor tm.pdf](crane%20winch%20motor%20tm.pdf)
Parker motor parts diagram                                        | [crane winch parts list.pdf](crane%20winch%20parts%20list.pdf)
Parker motor parts catalog                                        | [Parker PGP 020 Series Pumps and Motors.pdf](Parker%20PGP%20020%20Series%20Pumps%20and%20Motors.pdf)

