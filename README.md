# M939 Series 5-Ton 6x6 Truck Technical Manuals

This repository contains my curated cache of technical manuals for the
M939 series of 5 ton 6x6 military trucks.

![M923A2 Line Drawing](M923A2.png)

Vehicles in the series include:

Name                               | Model   | NSN
-----------------------------------|--------|-----------------
TRUCK, CARGO: 5-TON, 6X6 DROPSIDE  | M923   | 2320-01-050-2084
TRUCK, CARGO: 5-TON, 6X6 DROPSIDE  | M923A1 | 2320-01-206-4087
TRUCK, CARGO: 5-TON, 6X6 DROPSIDE  | M923A2 | 2320-01-230-0307
TRUCK, CARGO: 5-TON, 6X6 DROPSIDE  | M925   | 2320-01-047-8769
TRUCK, CARGO: 5-TON, 6X6 DROPSIDE  | M925A1 | 2320-01-206-4088
TRUCK, CARGO: 5-TON, 6X6 DROPSIDE  | M925A2 | 2320-01-230-0308
TRUCK, CARGO: 5-TON, 6X6 XLWB      | M927   | 2320-01-047-8771
TRUCK, CARGO: 5-TON, 6X6 XLWB      | M927A1 | 2320-01-206-4089
TRUCK, CARGO: 5-TON, 6X6 XLWB      | M927A2 | 2320-01-230-0309
TRUCK, CARGO: 5-TON, 6X6 XLWB      | M928   | 2320-01-047-8770
TRUCK, CARGO: 5-TON, 6X6 XLWB      | M928A1 | 2320-01-206-4090
TRUCK, CARGO: 5-TON, 6X6 XLWB      | M928A2 | 2320-01-230-0310
TRUCK, DUMP: 5-TON, 6X6            | M929   | 2320-01-047-8756
TRUCK, DUMP: 5-TON, 6X6            | M929A1 | 2320-01-206-4079
TRUCK, DUMP: 5-TON, 6X6            | M929A2 | 2320-01-230-0305
TRUCK, DUMP: 5-TON, 6X6            | M930   | 2320-01-047-8755
TRUCK, DUMP: 5-TON, 6X6            | M930A1 | 2320-01-206-4080
TRUCK, DUMP: 5-TON, 6X6            | M930A2 | 2320-01-230-0306
TRUCK, TRACTOR: 5-TON, 6X6         | M931   | 2320-01-047-8753
TRUCK, TRACTOR: 5-TON, 6X6         | M931A1 | 2320-01-206-4077
TRUCK, TRACTOR: 5-TON, 6X6         | M931A2 | 2320-01-230-0302
TRUCK, TRACTOR: 5-TON, 6X6         | M932   | 2320-01-047-8752
TRUCK, TRACTOR: 5-TON, 6X6         | M932A1 | 2320-01-205-2684
TRUCK, TRACTOR: 5-TON, 6X6         | M932A2 | 2320-01-230-0303
TRUCK, VAN, EXPANSIBLE: 5-TON, 6X6 | M934   | 2320-01-047-8750
TRUCK, VAN, EXPANSIBLE: 5-TON, 6X6 | M934A1 | 2320-01-205-2682
TRUCK, VAN, EXPANSIBLE: 5-TON, 6X6 | M934A2 | 2320-01-230-0300
TRUCK, MEDIUM WRECKER: 5-TON, 6X6  | M936   | 2320-01-047-8754
TRUCK, MEDIUM WRECKER: 5-TON, 6X6  | M936A1 | 2320-01-206-4078
TRUCK, MEDIUM WRECKER: 5-TON, 6X6  | M936A2 | 2320-01-230-0304

The documents are sorted into these subdirectories:


Directory              | Description
-----------------------|---------------------------------------
[TM](TM/README.md)     | Official US military Technical Manuals
[P2P](P2P/README.md)   | Point-to-point wiring diagram program
[misc](misc/README.md) | Miscellaneous documents

