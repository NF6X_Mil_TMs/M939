# Point to Point Program

This is an interactive wiring and plumbing diagram which was provided
to help maintenance personnel understand the M939 series trucks'
complex systems. It runs under older versions of Windows. I have not
tested it in Windows 10, and I have not yet succeeded in running it
under Wine.

To use it:

* Unpack the zip archive into a new folder on a Windows computer.
* Run the "runa6w32.exe" program.
* A file selector dialog should appear. Open file "Nav.a6r".

It's very useful for understanding the M939's electrical and pneumatic
systems. It's also kind of buggy. Sometimes it crashes.

