# M939 Series Technical Manuals

This directory contains official technical manuals for the M939 series trucks.

## Technical Manuals

Title                                         | Manual                                                   | Date
----------------------------------------------|----------------------------------------------------------|-----------
Operator's Manual                             | [TM 9-2320-272-10](TM9-2320-272-10_2012-07-31.pdf)       | 2012-07-31
Hand Receipts                                 | [TM 9-2320-272-10-HR](TM9-2320-272-10-HR_1990-04-30.pdf) | 1990-04-30
Field Maintenance Manual, Volume 1            | [TM 9-2320-272-23-1](TM9-2320-272-23-1_2012-09-10.pdf)   | 2012-09-10
Field Maintenance Manual, Volume 2            | [TM 9-2320-272-23-2](TM9-2320-272-23-2_2012-09-10.pdf)   | 2012-09-10
Field Maintenance Manual, Volume 3            | [TM 9-2320-272-23-3](TM9-2320-272-23-3_2012-09-10.pdf)   | 2012-09-10
Field Maintenance Manual, Volume 4            | [TM 9-2320-272-23-4](TM9-2320-272-23-4_2012-09-10.pdf)   | 2012-09-10
Field Maintenance Manual, Volume 5            | [TM 9-2320-272-23-5](TM9-2320-272-23-5_2012-09-10.pdf)   | 2012-09-10
Parts Manual, Volume 1                        | [TM 9-2320-272-24P-1](TM9-2320-272-24P-1_1999-02-01.pdf) | 1999-02-01
Parts Manual, Volume 2                        | [TM 9-2320-272-24P-2](TM9-2320-272-24P-2_1999-02-01.pdf) | 1999-02-01
Lubrication Order                             | [LO 9-2320-272-12](LO9-2320-272-12_1990-06-01.pdf)       | 1990-06-01
Transportability Guidance                     | [TM 55-2320-272-14-1](TM55-2320-272-14-1_1993-09-30.pdf) | 1993-09-30


## Modification Work Orders

Title                                         | Manual                                                   | Date
----------------------------------------------|----------------------------------------------------------|-----------
WRECKER AUTOMATIC THROTTLE KIT                | [MWO 9-2320-272-20-4](MWO9-2320-272-20-4_1994-07-27.pdf) | 1994-07-27
FRONT LIFTING PIN AND SHACKLE KIT             | [MWO 9-2320-272-20-5](MWO9-2320-272-20-5_1996-02-01.pdf) | 1996-02-01
FUEL TANK VENTILATION KIT                     | [MWO 9-2320-272-20-6](MWO9-2320-272-20-6_1996-02-01.pdf) | 1996-02-01
BRAKE PROPORTIONING VALVE KIT                 | [MWO 9-2320-272-20-7](MWO9-2320-272-20-7_1996-02-01.pdf) | 1996-02-01
ACCELERATOR LINKAGE MODIFICATION KIT          | [MWO 9-2320-272-20-8](MWO9-2320-272-20-8_1997-03-01.pdf) | 1997-03-01
SPARE TIRE CARRIER REPAIR KIT                 | [MWO 9-2320-272-23-1](MWO9-2320-272-23-1_2010-03-31.pdf) | 2010-03-31
SPARE TIRE CARRIER REPAIR KIT                 | [MWO 9-2320-272-23-2](MWO9-2320-272-23-2_2010-03-31.pdf) | 2010-03-31
SPRING BRAKE CONTROL VALVE IMPROVEMENT        | [MWO 9-2320-272-24-1](MWO9-2320-272-24-1_1991-09-21.pdf) | 1991-09-21


## Older Versions

Title                                         | Manual                                                   | Date
----------------------------------------------|----------------------------------------------------------|-----------
Operator's Manual                             | [TM 9-2320-272-10](TM9-2320-272-10_2004-09-30.pdf)       | 2004-09-30
Unit, DS, and GS Maintenance Manual, Volume 1 | [TM 9-2320-272-24-1](TM9-2320-272-24-1_1998-06-30.pdf)   | 1998-06-30
Unit, DS, and GS Maintenance Manual, Volume 2 | [TM 9-2320-272-24-2](TM9-2320-272-24-2_1998-06-30.pdf)   | 1998-06-30
Unit, DS, and GS Maintenance Manual, Volume 3 | [TM 9-2320-272-24-3](TM9-2320-272-24-3_1998-06-30.pdf)   | 1998-06-30
Unit, DS, and GS Maintenance Manual, Volume 4 | [TM 9-2320-272-24-4](TM9-2320-272-24-4_1998-06-30.pdf)   | 1998-06-30
